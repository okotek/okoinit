package init

import (
	"log"
	"path/filepath"

	"gocv.io/x/gocv"
)

/*
According to the gocv documentation, we could just do an "if ok := webcam.Read(&img); !ok{return}"
*/

//GetCamList checks the unit for v4l cameras
func GetCamList() []int {
	minCam := 0
	maxCam := 10
	var ret []int

	for iter := minCam; iter < maxCam; iter++ {

		tmp, err := gocv.VideoCaptureDevice(iter)
		if err == nil {
			ret = append(ret, iter)
		}
		tmp.Close()
	}

	return ret
}

//GetClassifierList checks the classifier directory for .xml files and returns a list to the classifier process
func GetClassifierList() []string {
	fileList, err := filepath.Glob("classifiers/*.xml")
	if err != nil {
		log.Fatal("Error in GetClassifierList function.\nHint: There should be a subdirectory in the same directory as the executable \ncalled \"classifiers.\" \nError:", err)
	}

	return fileList
}
