module gitlab.com/okotek/okoinit

go 1.17

require (
	gitlab.com/okotek/okoframe v0.0.0-20211201201714-a1848fe286de // indirect
	gocv.io/x/gocv v0.28.0
)
